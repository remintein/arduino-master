#ifndef F_CPU
#define F_CPU 16000000UL
#endif

#ifndef BAUD
#define BAUD 9600
#endif // BAUD

#include <avr/io.h>
#include <inttypes.h>
#include <avr/eeprom.h>
#include <avr/pgmspace.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <util/delay.h>
#include <util/setbaud.h>
#include "usart.h"
#include "mdb.h"
#include "uplink.h"
#include "billAccept.h"

uint8_t POLL_ADDRESS[10] = {0x0B, 0x12, 0x1A, 0x33, 0x42, 0x4A, 0x52, 0x5B, 0x62, 0x73};
uint8_t EXT_UART_BUFFER[37]; //incoming buffer for receive data from VMC
// static uint8_t index = 0;
MDB_Byte MDB_BUFFER[37]; //incoming buffer for receive data from MDB peripheral
static uint8_t EXT_UART_BUFFER_COUNT;
volatile int MDB_BUFFER_COUNT;

//MDB receiving flags
int rcvcomplete; //MDB message receive completed flag
int mdboverflow; //MDB message receive error flag

void MDB_Setup_bill()
{
  send_str_p(0, PSTR("MDB setup begin... \r\n"));
  // Set baud rate with setbaud.h
  UBRR3H = UBRRH_VALUE;
  UBRR3L = UBRRL_VALUE;
  // Disable USART rate doubler (arduino bootloader leaves it enabled...)
  UCSR3A &= ~(1 << U2X3);
  // Set 9600-9-N-1 UART mode
  UCSR3C = (0 << UMSEL01) | (0 << UMSEL00) | (0 << UPM01) | (0 << UPM00) | (0 << USBS0) | (1 << UCSZ01) | (1 << UCSZ00);
  UCSR3B |= (1 << UCSZ02); // 9bit
  // Enable rx/tx
  UCSR3B |= (1 << RXEN3) | (1 << TXEN3);
}

void bv_setup()
{
  EXT_UART_BUFFER[0] = 0x34;
  EXT_UART_BUFFER[1] = 0x00;
  EXT_UART_BUFFER[2] = 0x2f;
  EXT_UART_BUFFER[3] = 0x00;
  EXT_UART_BUFFER[4] = 0x2f;
  EXT_UART_BUFFER[5] = 0x92;
  EXT_UART_BUFFER_COUNT = 6;

  if ((EXT_UART_BUFFER_COUNT > 0) && EXT_ChecksumValidate())
  { //command received, checksum is valid, proceeding
    bool IsAddressValid = false;
    switch (EXT_UART_BUFFER[0] & ADDRESS_MASK)
    { //check first byte for peripheral address validity
    case ADDRESS_CHANGER:
      IsAddressValid = true;
      break;
    case ADDRESS_GATEWAY:
      IsAddressValid = true;
      break;
    case ADDRESS_DISPLAY:
      IsAddressValid = true;
      break;
    case ADDRESS_EMS:
      IsAddressValid = true;
      break;
    case ADDRESS_VALIDATOR:
      IsAddressValid = true;
      break;
    case ADDRESS_AVD:
      IsAddressValid = true;
      break;
    case ADDRESS_CD1:
      IsAddressValid = true;
      break;
    case ADDRESS_CD2:
      IsAddressValid = true;
      break;
    case ADDRESS_USD1:
      IsAddressValid = true;
      break;
    case ADDRESS_USD2:
      IsAddressValid = true;
      break;
    case ADDRESS_USD3:
      IsAddressValid = true;
      break;
    case ADDRESS_COIN1:
      IsAddressValid = true;
      break;
    case ADDRESS_COIN2:
      IsAddressValid = true;
      break;

    default:
      break;
    }
    if (IsAddressValid)
    { //command seems valid, let's try to send it to peripheral
      MDB_Byte AddressByte;
      int addrtx = 0;
      AddressByte.data = EXT_UART_BUFFER[0];
      AddressByte.mode = 0x01;
      memcpy(&addrtx, &AddressByte, 2);
      MDB_write(addrtx);
      for (int i = 1; i < EXT_UART_BUFFER_COUNT; i++)
      {
        MDB_write(EXT_UART_BUFFER[i]);
      }
      processresponse(EXT_UART_BUFFER[0] & ADDRESS_MASK);
    }
  }
}

void bv_deny()
{
  EXT_UART_BUFFER[0] = 0x35;
  EXT_UART_BUFFER[1] = 0x00;
  EXT_UART_BUFFER[2] = 0x35;
  // EXT_UART_BUFFER[3] = 0x00;
  // EXT_UART_BUFFER[4] = 0x1f;
  // EXT_UART_BUFFER[5] = 0x72;
  EXT_UART_BUFFER_COUNT = 3;

  if ((EXT_UART_BUFFER_COUNT > 0) && EXT_ChecksumValidate())
  { //command received, checksum is valid, proceeding
    bool IsAddressValid = false;
    switch (EXT_UART_BUFFER[0] & ADDRESS_MASK)
    { //check first byte for peripheral address validity
    case ADDRESS_CHANGER:
      IsAddressValid = true;
      break;
    case ADDRESS_GATEWAY:
      IsAddressValid = true;
      break;
    case ADDRESS_DISPLAY:
      IsAddressValid = true;
      break;
    case ADDRESS_EMS:
      IsAddressValid = true;
      break;
    case ADDRESS_VALIDATOR:
      IsAddressValid = true;
      break;
    case ADDRESS_AVD:
      IsAddressValid = true;
      break;
    case ADDRESS_CD1:
      IsAddressValid = true;
      break;
    case ADDRESS_CD2:
      IsAddressValid = true;
      break;
    case ADDRESS_USD1:
      IsAddressValid = true;
      break;
    case ADDRESS_USD2:
      IsAddressValid = true;
      break;
    case ADDRESS_USD3:
      IsAddressValid = true;
      break;
    case ADDRESS_COIN1:
      IsAddressValid = true;
      break;
    case ADDRESS_COIN2:
      IsAddressValid = true;
      break;

    default:
      break;
    }
    if (IsAddressValid)
    { //command seems valid, let's try to send it to peripheral
      MDB_Byte AddressByte;
      int addrtx = 0;
      AddressByte.data = EXT_UART_BUFFER[0];
      AddressByte.mode = 0x01;
      memcpy(&addrtx, &AddressByte, 2);
      MDB_write(addrtx);
      for (int i = 1; i < EXT_UART_BUFFER_COUNT; i++)
      {
        MDB_write(EXT_UART_BUFFER[i]);
      }
      processresponse(EXT_UART_BUFFER[0] & ADDRESS_MASK);
    }
  }
}

void bv_accept()
{
  EXT_UART_BUFFER[0] = 0x35;
  EXT_UART_BUFFER[1] = 0x01;
  EXT_UART_BUFFER[2] = 0x36;
  // EXT_UART_BUFFER[3] = 0x00;
  // EXT_UART_BUFFER[4] = 0x1f;
  // EXT_UART_BUFFER[5] = 0x72;
  EXT_UART_BUFFER_COUNT = 3;

  if ((EXT_UART_BUFFER_COUNT > 0) && EXT_ChecksumValidate())
  { //command received, checksum is valid, proceeding
    bool IsAddressValid = false;
    switch (EXT_UART_BUFFER[0] & ADDRESS_MASK)
    { //check first byte for peripheral address validity
    case ADDRESS_CHANGER:
      IsAddressValid = true;
      break;
    case ADDRESS_GATEWAY:
      IsAddressValid = true;
      break;
    case ADDRESS_DISPLAY:
      IsAddressValid = true;
      break;
    case ADDRESS_EMS:
      IsAddressValid = true;
      break;
    case ADDRESS_VALIDATOR:
      IsAddressValid = true;
      break;
    case ADDRESS_AVD:
      IsAddressValid = true;
      break;
    case ADDRESS_CD1:
      IsAddressValid = true;
      break;
    case ADDRESS_CD2:
      IsAddressValid = true;
      break;
    case ADDRESS_USD1:
      IsAddressValid = true;
      break;
    case ADDRESS_USD2:
      IsAddressValid = true;
      break;
    case ADDRESS_USD3:
      IsAddressValid = true;
      break;
    case ADDRESS_COIN1:
      IsAddressValid = true;
      break;
    case ADDRESS_COIN2:
      IsAddressValid = true;
      break;

    default:
      break;
    }
    if (IsAddressValid)
    { //command seems valid, let's try to send it to peripheral
      MDB_Byte AddressByte;
      int addrtx = 0;
      AddressByte.data = EXT_UART_BUFFER[0];
      AddressByte.mode = 0x01;
      memcpy(&addrtx, &AddressByte, 2);
      MDB_write(addrtx);
      for (int i = 1; i < EXT_UART_BUFFER_COUNT; i++)
      {
        MDB_write(EXT_UART_BUFFER[i]);
      }
      processresponse(EXT_UART_BUFFER[0] & ADDRESS_MASK);
    }
  }
}

void bv_stack()
{
  EXT_UART_BUFFER[0] = 0x36;
  EXT_UART_BUFFER[1] = 0x36;
  // EXT_UART_BUFFER[2] = 0x36;
  // EXT_UART_BUFFER[3] = 0x00;
  // EXT_UART_BUFFER[4] = 0x1f;
  // EXT_UART_BUFFER[5] = 0x72;
  EXT_UART_BUFFER_COUNT = 2;

  if ((EXT_UART_BUFFER_COUNT > 0) && EXT_ChecksumValidate())
  { //command received, checksum is valid, proceeding
    bool IsAddressValid = false;
    switch (EXT_UART_BUFFER[0] & ADDRESS_MASK)
    { //check first byte for peripheral address validity
    case ADDRESS_CHANGER:
      IsAddressValid = true;
      break;
    case ADDRESS_GATEWAY:
      IsAddressValid = true;
      break;
    case ADDRESS_DISPLAY:
      IsAddressValid = true;
      break;
    case ADDRESS_EMS:
      IsAddressValid = true;
      break;
    case ADDRESS_VALIDATOR:
      IsAddressValid = true;
      break;
    case ADDRESS_AVD:
      IsAddressValid = true;
      break;
    case ADDRESS_CD1:
      IsAddressValid = true;
      break;
    case ADDRESS_CD2:
      IsAddressValid = true;
      break;
    case ADDRESS_USD1:
      IsAddressValid = true;
      break;
    case ADDRESS_USD2:
      IsAddressValid = true;
      break;
    case ADDRESS_USD3:
      IsAddressValid = true;
      break;
    case ADDRESS_COIN1:
      IsAddressValid = true;
      break;
    case ADDRESS_COIN2:
      IsAddressValid = true;
      break;

    default:
      break;
    }
    if (IsAddressValid)
    { //command seems valid, let's try to send it to peripheral
      MDB_Byte AddressByte;
      int addrtx = 0;
      AddressByte.data = EXT_UART_BUFFER[0];
      AddressByte.mode = 0x01;
      memcpy(&addrtx, &AddressByte, 2);
      MDB_write(addrtx);
      for (int i = 1; i < EXT_UART_BUFFER_COUNT; i++)
      {
        MDB_write(EXT_UART_BUFFER[i]);
      }
      processresponse(EXT_UART_BUFFER[0] & ADDRESS_MASK);
    }
  }
}

void MDB_checksumGenerate()
{
  uint8_t sum = 0;
  for (int i = 0; i < (EXT_UART_BUFFER_COUNT); i++)
    sum += EXT_UART_BUFFER[i];
  EXT_UART_BUFFER[EXT_UART_BUFFER_COUNT++] = (sum & 0xFF); //only first 8 bits are checksum
}

void MDB_write(int data)
{
  MDB_Byte b;
  memcpy(&b, &data, 2);
  write_9bit(b);
}

void write_9bit(MDB_Byte mdbb)
{
  while (!(UCSR3A & (1 << UDRE3)))
    ;
  if (mdbb.mode)
  {
    //turn on bit 9
    UCSR3B |= (1 << TXB83);
  }
  else
  {
    //turn off bit 9
    UCSR3B &= ~(1 << TXB83);
  }
  UDR3 = mdbb.data;
}

int MDB_Receive()
{
  unsigned char resh, resl;
  // char tmpstr[64];
  int rtr = 0;
  // Wait for data to be received
  while ((!(UCSR3A & (1 << RXC3))) && rtr < 50)
  {
    _delay_ms(1);
    rtr++;
  }
  if (rtr == 50)
  {
    mdboverflow = 1;
    rcvcomplete = 1;
  }
  // Get 9th bit, then data from buffer
  resh = UCSR3B;
  resl = UDR3;
  // Filter the 9th bit, then return only data w\o mode bit
  resh = (resh >> 1) & 0x01;
  return ((resh << 8) | resl);
}

void MDB_getByte(MDB_Byte *mdbb)
{
  int b;
  b = 0;
  b = MDB_Receive();
  memcpy(mdbb, &b, 2);
}

uint8_t EXT_ChecksumValidate()
{
  uint8_t sum = 0;
  for (int i = 0; i < (EXT_UART_BUFFER_COUNT - 1); i++)
    sum += EXT_UART_BUFFER[i];
  if (EXT_UART_BUFFER[EXT_UART_BUFFER_COUNT - 1] == (sum & 0xFF))
    return 1;
  else
    return 0;
}

uint8_t MDB_ChecksumValidate()
{
  int sum = 0;
  for (int i = 0; i < (MDB_BUFFER_COUNT - 1); i++)
    sum += MDB_BUFFER[i].data;
  if (MDB_BUFFER[MDB_BUFFER_COUNT - 1].data == (sum & 0xFF))
    return 1;
  else
    return 0;
}

void MDB_read()
{
  MDB_getByte(&MDB_BUFFER[MDB_BUFFER_COUNT]);
  MDB_BUFFER_COUNT++;
  if (MDB_BUFFER_COUNT == 35)
  {
    rcvcomplete = 1;
    mdboverflow = 1;
  }
  if (MDB_BUFFER[MDB_BUFFER_COUNT - 1].mode && MDB_ChecksumValidate())
  {
    rcvcomplete = 1;
  }
}

void MDBFlush()
{
  MDB_BUFFER_COUNT = 0;
  fflush(stdin);
  fflush(stdout);
  // Serial.flush();
}

void processresponse(int addr)
{
  mdboverflow = 0;
  rcvcomplete = 0;
  while (!rcvcomplete)
  {
    MDB_read();
  }
  if ((rcvcomplete) && (!mdboverflow))
  {
    if (MDB_BUFFER_COUNT > 1)
    {
      MDB_write(0x00);
      // send_mdb(BILL_USART,0x100); // send ACK to MDB if peripheral answer is not just *ACK*, otherwise peripheral will try to send unconfirmed data with next polls
    }
    else
    {
      if (MDB_BUFFER_COUNT == 1)
      {
        //just *ACK* received from peripheral device, no confirmation needed
      }
    }
    //finally, send data from peripheral to VMC via serial port as string representation of hex bytes
    char buff[5];
    sprintf(buff, "%02x ", addr);
    send_str(UPLINK_USART, buff);
    for (int a = 0; a < MDB_BUFFER_COUNT - 1; a++)
    {
      sprintf(buff, "%02x ", MDB_BUFFER[a].data);
      send_str(0, buff);
    }
    //last byte representation will be sent without following space but with EOL to easy handle
    sprintf(buff, "%02x\r\n", MDB_BUFFER[MDB_BUFFER_COUNT - 1].data);
    send_str(0, buff);
  }
}

void PollDevice(uint8_t devaddrbyte)
{
  MDB_Byte addrbyte;
  rcvcomplete = 0;
  int addrtx = 0;
  addrbyte.data = devaddrbyte;
  addrbyte.mode = 0x01;
  memcpy(&addrtx, &addrbyte, 2);
  MDB_write(addrtx);
  MDB_write(addrbyte.data);
  processresponse(addrbyte.data & ADDRESS_MASK);
}

void ResetDevice(uint8_t resetCmd)
{
  MDB_Byte addrbyte;
  rcvcomplete = 0;
  int addrtx = 0;
  addrbyte.data = resetCmd;
  addrbyte.mode = 0x01;
  memcpy(&addrtx, &addrbyte, 2);
  MDB_write(addrtx);
  MDB_write(addrbyte.data);
  processresponse(addrbyte.data & ADDRESS_MASK);
}