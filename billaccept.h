#ifndef BILLACCEPT_H
#define BILLACCEPT_H

#ifndef F_CPU
#define F_CPU 16000000UL
#endif //F_CPU

#ifndef BAUD
#define BAUD 9600
#endif // BAUD

#define BILL_USART 3

#define ADDRESS_MASK (0xF8) // section 2.3 - top five bits of address are actually address
#define COMMAND_MASK (0x07) // section 2.3 - bottom three bits are command

// Address of each peripheral device type (MDB Version 4.2, page 25)
#define ADDRESS_VMC (0x00)			 // Reserved!!!
#define ADDRESS_CHANGER (0x08)	 // Coin Changer
#define ADDRESS_CD1 (0x10)			 // Cashless Device 1
#define ADDRESS_GATEWAY (0x18)	 // Communication Gateway
#define ADDRESS_DISPLAY (0x20)	 // Display
#define ADDRESS_EMS (0x28)			 // Energy Management System
#define ADDRESS_VALIDATOR (0x30) // Bill Validator
#define ADDRESS_USD1 (0x40)			 // Universal satellite device 1
#define ADDRESS_USD2 (0x48)			 // Universal satellite device 2
#define ADDRESS_USD3 (0x50)			 // Universal satellite device 3
#define ADDRESS_COIN1 (0x58)		 // Coin Hopper 1
#define ADDRESS_CD2 (0x60)			 // Cashless Device 2
#define ADDRESS_AVD (0x68)			 // Age Verification Device
#define ADDRESS_COIN2 (0x70)		 // Coin Hopper 2

typedef struct MDB_Byte
{
	uint8_t data;
	uint8_t mode;
} MDB_Byte;

typedef enum
{
	false,
	true
} bool;

void MDB_Setup_bill(void);
// void EXT_UART_read(void);
void bv_setup(void);
void bv_accept(void);
void bv_deny(void);
void bv_stack(void);
void MDB_checksumGenerate(void);
void MDB_write(int data);
void write_9bit(MDB_Byte mdbb);
int MDB_Receive(void);
void MDB_getByte(MDB_Byte *mdbb);
uint8_t EXT_ChecksumValidate(void);
uint8_t MDB_ChecksumValidate(void);
void MDB_read(void);
void MDBFlush(void);
void processresponse(int addr);
void PollDevice(uint8_t devaddrbyte);
void ResetDevice(uint8_t resetCmd);

#endif
