/**
 * Project: MateDealer
 * File name: main.c
 * Description:  Main file of the MateDealer Project
 *   
 * @author bouni
 * @email bouni@owee.de  
 *   
 */

#ifndef F_CPU
#define F_CPU 16000000UL
#endif

#ifndef BAUD
#define BAUD 9600
#endif // BAUD

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <util/delay.h>
#include <util/setbaud.h>
#include "usart.h"
#include "mdb.h"
#include "uplink.h"
#include "billaccept.h"

extern volatile int MDB_BUFFER_COUNT;

int main(void)
{
	send_str_p(0, PSTR("Setup begin... \r\n"));
	MDB_Setup_bill();
	setup_usart(0, 38400, 8, 'N', 1);
	setup_usart(1, 9600, 9, 'N', 1);

	_delay_ms(500);
	MDB_BUFFER_COUNT = 0;
	// ResetDevice(0x30);

	sei(); //Set global interrupt enabled

	// send_str_p(0, PSTR("MateDealer is up and running\r\n"));
	_delay_ms(500);

	// Main Loop
	while (1)
	{
		MDBFlush();
		PollDevice(0x33);
		MDBFlush();
		mdb_cmd_handler();
		uplink_cmd_handler();
		_delay_ms(200);
	}
	return 0;
}
